/* Author: Craig Scheiderer
 * Date: 4/17/2017
 * Description: 
 */

import java.io.FileReader;
import java.io.BufferedReader;
import java.util.Scanner;
import java.io.IOException;
import java.util.ArrayList;

public class csvParser {
	
	ArrayList<Device> csvList = new ArrayList<Device>();
	FileReader filereader;
	BufferedReader bufferedreader;
	Scanner scanner;

	public ArrayList<Device> parseCsv(String csvfilePath){
		
		try {
			filereader = new FileReader(csvfilePath);
			bufferedreader = new BufferedReader(filereader);
			scanner = new Scanner(bufferedreader);
			
			while (scanner.hasNextLine()){
				String[] deviceData = scanner.nextLine().split(",");
				
				//skip over the first line containing the data schema
				if (!deviceData[0].equals("uuid")){
					//pull parameters for Device object from String split array
					String uuid = deviceData[0];
					double latitude = Double.parseDouble(deviceData[1]);
					double longitude = Double.parseDouble(deviceData[2]);
					long timestamp = Long.parseLong(deviceData[3]);
					//create a new Device and add to the array list
					Device device = new Device(uuid, latitude, longitude, timestamp);
					csvList.add(device);
				}	
			}				
		} catch (IOException exc) {
			exc.getMessage();
		}
		return csvList;
	}
}
